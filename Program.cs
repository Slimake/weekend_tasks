﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weekend_task
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter random words");
            var words = Console.ReadLine().Trim();

            while (String.IsNullOrEmpty(words))
            {
                Console.WriteLine("Field cannot be blank");
                words = Console.ReadLine().Trim();
            }

            var splitWord = words.Split();

            List<string> sortedList = new List<string>();

            foreach(var word in splitWord)
            {
                sortedList.Add(word);
            }

            sortedList.Sort();

            Console.WriteLine("\nSorted words:");
            foreach (var word in sortedList)
            {
                Console.Write($"{word} ");
            }

            //Query syntax
            var querySyntax = from word in sortedList
                              orderby Shuffle()
                              select word;

            Console.WriteLine("\n\nQuery syntax shuffled words:");
            foreach (var word in querySyntax)
            {
                Console.Write($"{word} ");
            }

            //Method syntax
            var methodSyntax = sortedList.OrderBy(word => Shuffle());

            Console.WriteLine("\n\nMethod syntax shuffled words:");
            foreach (var word in methodSyntax)
            {
                Console.Write($"{word} ");
            }

            int Shuffle()
            {
                var random = new Random();
                var shuffle = random.Next(splitWord.Length);
                return shuffle;
            }

            Console.ReadLine();
        }

    }
}
